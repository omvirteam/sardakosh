<?php namespace App\Http\Controllers;

use Elasticsearch\Client as Es;
use Illuminate\Http\Request;


class SearchService extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{

	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */

	public function test() {
		// $es = new Es;
		return response()->json(array('status' => "Cool"));
	}

	public function createArticle(Request $request) {

		// $body = array(
  //     '_source' => array(
  // 			'title'=> $request->has('title') ? $request->get('title'): "",
  // 			'text' => $request->has('text') ? $request->get('text'): ""));

		// var_dump($body); exit;

		$es = new Es;

    $params['body']  = array(
      'title' => $request->has('title') ? $request->get('title'): "",
      'author' => 'tester',
      'text'=> $request->has('text') ? $request->get('text'): "");

    $params['index'] = 'shardakosh';
    $params['type']  = 'articles';
    if($request->has('id')) {
      $params['id']    = $request->get('id');
    }

    $ret = $es->index($params);

		return response()->json($ret);
	}

  public function getArticles(Request $request) {
    $es = new Es;

    $params['index'] = 'shardakosh';
    $params['type']  = 'articles';

    $ret = $es->search($params);

    return response()->json($ret);
  }

  public function deleteArticles($id, Request $request) {
    $es = new Es;

    $params['index'] = 'shardakosh';
    $params['type']  = 'articles';
    $params['id']  = $id;

    $ret = $es->delete($params);

    return response()->json($ret);

  }


  public function getAllTagTypes() {

    $es = new Es;

    $params['index'] = 'shardakosh';
    $params['type']  = 'tagtypes';

    $ret = $es->search($params);

    return response()->json($ret);

  }



  public function addTagType(Request $request) {

    $es = new Es;

    $params['index'] = 'shardakosh';
    $params['type']  = 'tagtypes';
    // if($request->has('code')) {
    //   $params['id']  = $request->get('code');
    // }

    $params['body']  = array('title' => $request->has('title') ? $request->get('title'): "");

    $ret = $es->index($params);

    return response()->json($ret);

  }



  public function getTagsList() {
    $es = new Es;
    $params['index'] = 'shardakosh';
    $params['type']  = 'tags';
    $ret = $es->search($params);
    return response()->json($ret);
  }

  public function saveTag(Request $request) {
    $es = new Es;
    $params['index'] = 'shardakosh';
    $params['type']  = 'tags';
    $params['body']  = $request->all();
    $ret = $es->index($params);
    return response()->json($ret);

  }

  public function delTag($id, Request $request) {
    $es = new Es;
    $params['index'] = 'shardakosh';
    $params['type']  = 'tags';
    $params['id']  = $id;
    $ret = $es->delete($params);
    return response()->json($ret);
  }


  public function getTagsGrp($tagGrp) {
    $es = new Es;
    $params['index'] = 'shardakosh';
    $params['type']  = 'tagsGrp';
    $params['id']  = $tagGrp;
    // return response()->json($params);
    $ret = $es->get($params);
    return response()->json($ret);
  }

  public function getTagsGrpList() {
    $es = new Es;
    $params['index'] = 'shardakosh';
    $params['type']  = 'tagsGrp';
    $ret = $es->search($params);
    return response()->json($ret);
  }

  public function saveTagGrp(Request $request) {
    $es = new Es;
    $params['index'] = 'shardakosh';
    $params['type']  = 'tagsGrp';
    $params['body']  = $request->all();
    $ret = $es->index($params);
    return response()->json($ret);

  }

  public function delTagGrp($id, Request $request) {
    $es = new Es;
    $params['index'] = 'shardakosh';
    $params['type']  = 'tagsGrp';
    $params['id']  = $id;
    $ret = $es->delete($params);
    return response()->json($ret);
  }


}
