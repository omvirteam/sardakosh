<?php namespace App\Http\Controllers;

use Elasticsearch\Client as Es;

class WelcomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('welcome');
	}

	public function test()
	{
		$searchParams['index'] = 'shardakosh';
		$searchParams['size'] = 1;
		$searchParams['body']['query']['query_string']['query'] = 'name:sample';

		$es = new Es;
		$result = $es->search($searchParams);
		return view('test-page', array('result' => $result));
	}

	public function admin_old() {
		return view('admin_old');
	}

	public function admin() {
		return view('admin');
	}

}
