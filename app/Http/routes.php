<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);


Route::get('/test', 'WelcomeController@test');

Route::get('/admin_old', 'WelcomeController@admin_old');

Route::get('/admin', 'WelcomeController@admin');

Route::get('/search', 'WelcomeController@search');

Route::get('/ssTest', 'SearchService@test');

Route::post('/article/create', 'SearchService@createArticle');

Route::get('/article/all', 'SearchService@getArticles');

Route::get('/article/delete/{id}', 'SearchService@deleteArticles');


Route::post('/service/tagType/add', 'SearchService@addTagType');

Route::get('/service/tagType/all', 'SearchService@getAllTagTypes');


Route::get('/service/tags/list', 'SearchService@getTagsGrpList');
Route::post('/service/tags/save', 'SearchService@saveTagGrp');
Route::get('/service/tags/del/{id}', 'SearchService@delTagGrp');

Route::get('/service/tags/{tagGrp}', 'SearchService@getTagsGrp');
Route::get('/service/tags/{tagGrp}/list', 'SearchService@getTagsList');
Route::post('/service/tags/{tagGrp}/save', 'SearchService@saveTag');
Route::get('/service/tags/{tagGrp}/del/{id}', 'SearchService@delTag');


