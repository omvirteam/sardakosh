<!DOCTYPE html>
<html data-ng-app="testApp">
  <head>
    <title>Laravel</title>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

    <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="/css/admin.css" />
  </head>
  <body>
    <div class="container container-main" data-ng-controller="AdminController">

      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">CodeGeeks</a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
              <li data-ng-repeat="nav in navList" data-ng-class="getClass(nav.path)"><a href="<%nav.path%>"><%nav.title%></a></li>
            </ul>
          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
      </nav>


    </div>

    <div class="container" data-ng-view>
    </div>


    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.js"></script>
    <script src=" //cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.15/angular-route.min.js" type="text/javascript"></script>
    <script src="/js/admin.js" type="text/javascript"></script>
  </body>
</html>
