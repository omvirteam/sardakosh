<!DOCTYPE html>
<html data-ng-app="testApp">
	<head>
		<title>Laravel</title>

		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

		<link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	</head>
	<body>
		<div class="container">
			<div class="content" data-ng-controller="Testing">
				<input class="pull-right" type="text" data-ng-model="user" />
				<h1><%greeting%> <% user != null ? user: "Dear" %> </h1>

				<br/>
				<br/>

				<table class="table table-bordered">
					<thead>
						<tr>
							<th>Scope</th>
							<th>Applies</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Testing <button data-ng-click="ssTest()">Click me</button></td>
							<td> <% status %> </td>
						</tr>
						<tr>
							<td>
								<input id="newArticleTitleInput" class="form-control" type="text" data-ng-model="newArticle.title" /><br/>
								<textarea class="form-control" data-ng-model="newArticle.text" ></textarea><br/>
							 <button class="form-control" data-ng-click="createArticle()">Click me</button></td>
							<td> <% createStatus %> </td>
						</tr>
						<tr>
							<td><button data-ng-click="getArticle()">Load Articles <span data-ng-show="articleCount"> (<% articleCount %>)</span></button></td>
							<td>
								<div data-ng-hide="articleList.length">-- Nothing --</div>
								<div data-ng-show="articleList.length">
									<div data-ng-repeat="article in articleList"><% article._source.title %>
										<button class="pull-right" data-ng-click="deleteArticle(article)">Delete</button>
										<button class="pull-right" data-ng-click="editArticle(article)">Edit</button>&nbsp;
										<div style="clear:both"></div>
									</div>
								</div>
							</td>
						</tr>
					</tbody>
				</table>

			</div>
		</div>
		<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
		<script src="/js/app.js" type="text/javascript"></script>
	</body>
</html>
