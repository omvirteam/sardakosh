<!DOCTYPE html>
<html data-ng-app="testApp">
  <head>
    <title>Laravel</title>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

    <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  </head>
  <body>
    <div class="container">
      <div class="content" data-ng-controller="AdminController">
        <h1>Admin panel</h1>
        <div class="row">
          <div class="col-md-4">
            <h1><small>Tags</small></h1>
              <div data-ng-repeat="tType in tagTypes">
                <h4><%tType._source.title%></h4>
              </div>
              <div class="input-group">
                <input id="newTagType" type="text" class="form-control" placeholder="New code for type ..." data-ng-model="newTagType">
                <span class="input-group-btn">
                  <button class="btn btn-default" type="button" data-ng-click="addTagType()">Add!</button>
                </span>
              </div><!-- /input-group -->
          </div>
          <div class="col-md-8">
            <h1><small>Events</small></h1>
          </div>
        </div>
      </div>
    </div>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
    <script src="/js/admin_old.js" type="text/javascript"></script>
  </body>
</html>
