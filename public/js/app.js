var testApp = angular.module('testApp', [], function($interpolateProvider) {
  $interpolateProvider.startSymbol('<%');
  $interpolateProvider.endSymbol('%>');
});

testApp.service('SearchService', function($http) {

  this.testSearchService = function(callback) {
    $http.get('/ssTest').then(callback);
  }

  this.createArticle = function(article, callback) {
    $http.post('/article/create', article).then(callback);
  }

  this.getAllArticle = function(callback) {
    $http.get('/article/all').then(callback);
  }

  this.deleteArticle = function(article, callback) {
    $http.get('/article/delete/' + article._id).then(callback); 
  }

})

testApp.controller('Testing', function($scope, SearchService) {

  $scope.greeting = null;
  $scope.user = null;
  $scope.newArticle = null;
  $scope.status = "initial";
  $scope.createStatus = "initial";
  $scope.articleCount = 0;
  $scope.articleList = [];


  var init = function() {

    $scope.greeting = "Hello";
    $scope.user = "Hari";
    $scope.status = "not called service yet";
    $scope.createStatus = "not called service yet";

  }

  init();
  
  $scope.ssTest = function() {
    SearchService.testSearchService(function(res) {
      $scope.status = res.data.status;
    });
  };

  $scope.createArticle = function() {
    SearchService.createArticle($scope.newArticle, function(res) {
      if(res.data.created) {
        $scope.createStatus = "Created";
      } else {
        $scope.createStatus = "Updated";
      }
      $scope.newArticle = {};
      $("#newArticleTitleInput").focus();
    });
  };

  $scope.getArticle = function(id) {
    if(id == undefined) {
      SearchService.getAllArticle(function(res) {
        $scope.articleCount = res.data.hits.total;
        $scope.articleList = res.data.hits.hits;
        console.log(res.data.hits.hits);
      });
    };
  }

  $scope.editArticle = function(article) {
    $scope.newArticle = article._source;
    $scope.newArticle['id'] = article._id;
    $("#newArticleTitleInput").focus();
  }

  $scope.deleteArticle = function(article) {
    if(confirm("Are you sure you want to delete this article?")) {
      $scope.articleList.splice($scope.articleList.indexOf(article), 1);
      SearchService.deleteArticle(article, function(res) {
        console.log(res);
      });
    }
  }

});