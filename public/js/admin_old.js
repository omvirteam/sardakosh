var testApp = angular.module('testApp', [], function($interpolateProvider) {
  $interpolateProvider.startSymbol('<%');
  $interpolateProvider.endSymbol('%>');
});

testApp.service('SearchService', function($http) {

});

testApp.service('AdminService', function($http) {

  this.addTagType = function(newTagType, callback) {
    $http.post('/service/tagType/add', newTagType).then(callback);
  };

  this.allTagType = function(callback) {
    $http.get('/service/tagType/all').then(callback); 
  };

});

testApp.controller('AdminController', function($scope, SearchService, AdminService) {

  $scope.tagTypes = [];

  var init = function() {
    AdminService.allTagType(function(res) {
      $scope.tagTypes = res.data.hits.hits;
    });
  };

  init();

  $scope.addTagType = function() {

    var newTagType = {
      title: $scope.newTagType
    };
    $scope.tagTypes.push(newTagType);
    $scope.newTagType = '';
    // $scope.newTagTypeCode = '';
    $("#newTagType").focus();

    AdminService.addTagType(newTagType, function(res) {
      AdminService.allTagType(function(res) {
        $scope.tagTypes = res.data.hits.hits;
      });
    });

  };
  

});