
// -- CONFIG --
var testApp = angular.module('testApp', ['ngRoute'], function($interpolateProvider) {
  $interpolateProvider.startSymbol('<%');
  $interpolateProvider.endSymbol('%>');
});

testApp.config(function($routeProvider) {
  $routeProvider
    .when('/events', { templateUrl: 'html/events.html'})
    .when('/tags', { templateUrl: 'html/tags_home.html' })
    .when('/tags/:tagGrpId', { templateUrl: 'html/tags_grp.html' })
    .when('/login/:type', { templateUrl: 'angular/views/login.html' })
    .when('/thanks', { templateUrl: 'angular/views/registrationThanks.html' })
    .when('/loading', { templateUrl: 'loading.html' })
    .otherwise({ redirectTo: '/' });
});

// -- SERVICES --
testApp.service('AdminService', function($http) {

  this.addTagType = function(newTagType, callback) {
    $http.post('/service/tagType/add', newTagType).then(callback);
  };

  this.allTagType = function(callback) {
    $http.get('/service/tagType/all').then(callback); 
  };

});

testApp.service('TagsService', function($http) {

  this.saveTagGrp = function(newTag, callback) {
    $http.post('/service/tags/save', newTag).then(callback);
  };

  this.delTagGrp = function(tag, callback) {
    $http.get('/service/tags/del/' + tag._id).then(callback);
  }

  this.allTagGrp = function(callback) {
    $http.get('/service/tags/list').then(callback); 
  };



  this.getTagGrp = function(id, callback) {
    $http.get('/service/tags/'+id).then(callback); 
  }

  this.saveTagType = function(id, newTag, callback) {
    $http.post('/service/tags/'+id+'/save', newTag).then(callback);
  };

  this.delTagType = function(id, tag, callback) {
    $http.get('/service/tags/'+id+'/del/' + tag._id).then(callback);
  }

  this.allTagType = function(id, callback) {
    $http.get('/service/tags/'+id+'/list').then(callback); 
  };

});

// -- CONTROLLERS --
testApp.controller('AdminController', function($scope, AdminService) {

  $scope.navList = [];
  $scope.tagTypes = [];

  var init = function() {
    $scope.navList = [{
      'path'  : "#/events",
      'title' : "Events"
    }, {
      'path'  : "#/tags",
      'title' : "Tags"
    }];
  };

  init();

  $scope.getClass = function(link) { 
    return (link == window.location.hash ? "active": "");
  }  

});

testApp.controller('TagsGrpController', function($scope, $routeParams, TagsService) {
  
  $scope.tagsGrpList = [];
  $scope.newTagGrp = "";

  var init = function() {
    $scope.newTagGrp = "";
    TagsService.allTagGrp(function(data) {
      console.log(data);
      if(data.data.hits.hasOwnProperty('hits')) {
        $scope.tagsGrpList = data.data.hits.hits;
      }
    });
  }

  init();

  $scope.addNewTagGrp = function() {
    if($scope.newTagGrp.trim() != "") {
      var tag = {
        title: $scope.newTagGrp
      };
      TagsService.saveTagGrp(tag, function(data) {
        console.log(data);
        if(data.statusText == "OK") {
          data.data['_source'] = tag;
          $scope.tagsGrpList.push(data.data);
        }
        $scope.newTagGrp = "";
        $("#newTag").focus();
      });
    }
  }

  $scope.delTagGrp = function(item) {

    var removedItem = $scope.tagsGrpList.splice($scope.tagsGrpList.indexOf(item), 1);
    TagsService.delTagGrp(item, function(data) {
      if(data.statusText != "OK") {
        alert("Removal Unsuccessfull!");
        $scope.tagsGrpList.concat(removedItem);
      }
    });
  }

});






testApp.controller('TagsGrpInstController', function($scope, $routeParams, TagsService) {
  
  $scope.tagsTypeList = [];
  $scope.newTagType = "";
  $scope.tagGrp = "";

  var init = function() {
    TagsService.getTagGrp($routeParams.tagGrpId, function(res) {
      $scope.tagGrp = res.data;
      console.log(res);
    });
  }

  init();

});